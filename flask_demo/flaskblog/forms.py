from flask_wtf import FlaskForm    # importing FlaskForm from flask_wtf
from wtforms import StringField, SelectField, SubmitField, TextAreaField  # importing all the fields from wtforms
from wtforms.validators import DataRequired, Length, ValidationError # importint all the validation from wtfroms 
from flaskblog.model import Projects, Tasks # importing Projects and Tasks database from flaskblog passkage in model.py file 
# this project form all atribute has some validation
class ProjectForm(FlaskForm):
    proj_name = StringField('Project Name *',
                           validators=[DataRequired(), Length(min=2, max=30)]) # it is not null and min length is 2 and max is 30
    description = TextAreaField('Description *',
                        validators=[DataRequired() , Length(min=5, max=120)]) # it is not null and min length is 5 and max is 120
    start_date = StringField('Start date *', validators=[DataRequired()])  #it is not null
    end_date = StringField('End date *', validators=[DataRequired()]) # it is not null
    status = SelectField('Status *', choices=[('Started' , 'Started'),('Completed','Completed')], validators=[DataRequired()]) #it is also not null
    
    submit = SubmitField('Project Save')  # submit field
    # checking project name is unique or not 
    # def validate_proj_name(self, projname):
    #     proj = Projects.query.filter_by(proj_name=projname.data).first()
    #     if proj:
    #         raise ValidationError('That Project is taken Please choose a different Name.')
 
 # same as the project from 
class TaskForm(FlaskForm):
    task_title = StringField('Task title *',
                           validators=[DataRequired(), Length(min=2, max=30)])
    task_description =TextAreaField('Task Description *',
                        validators=[DataRequired() , Length(min=5, max=120)])
    estimate_hr = StringField('Estimate Hour *', validators=[DataRequired()])
    acctual_hr = StringField('Acctual Hour *', validators=[DataRequired()])
    comments = StringField('Comments')
    status = SelectField('Status *', choices=[('Open' , 'Open'),('Closed','Closed')], validators=[DataRequired()])
    assign_to = SelectField('Assign To *', choices=[('Persion 1' , 'Persion 1'),('Persion 2','Persion 2'),('Persion 3','Persion 3'),('Persion 4','Persion 4')], validators=[DataRequired()])
    project =  SelectField(choices=[(c.id, c.proj_name) for c in Projects.query.all()]) # here taking project name from database
    completed_date = StringField('Completed Date *')
   
    submit = SubmitField('Task Save')
    #checking task title is unique or not
    # def validate_task_title(self, tasktitle):
    #     user = Tasks.query.filter_by(task_title=tasktitle.data).first()
    #     if user:
    #         raise ValidationError(f"That Task Title  is taken Please choose a different one.")

    def validate_estimate_hr(self,extimate_hr):
      split_extimate_hr= extimate_hr.data.split()
      if  int(split_extimate_hr[0]) <=0:
         raise ValidationError(f"This {extimate_hr} is not valid Estimated hour.")
       
    def validate_acctual_hr(self,acctual_hr):
      split_acctual_hr= acctual_hr.data.split()
      if  int(split_acctual_hr[0]) <=0:
         raise ValidationError(f"This {acctual_hr} is not valid Acctual hour.")



