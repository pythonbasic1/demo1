from flaskblog import db  # importing db from  flaskblog pakkage 

'''
In this pyhton file holding the database model. there is two database 
1. Projects
in the project database it has 6 attribute and this is parrent class of tasks 
2. Tasks
in the tasks databse it 10 attribute and this database is connected through forren key
'''

class Projects(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    proj_name = db.Column(db.String(30), unique=True, nullable=False) # Project name is not nll and unique
    description = db.Column(db.String(120), nullable=False) # not null
    start_date = db.Column(db.String(30), nullable=False) # starting date not null
    end_date = db.Column(db.String(30), nullable=False) # not null
    status = db.Column(db.String(30), nullable=False) #not null
    tasks = db.relationship('Tasks', backref='author', lazy=True)

    # here checking project name unique 
    def __repr__(self):
        return f"Projects('{self.id}','{self.proj_name}', '{self.description}', '{self.start_date}','{self.end_date}'),'{self.status}'"


class Tasks(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    task_title = db.Column(db.String(50), nullable=False)  # task title is not null
    task_description = db.Column(db.String(120), nullable=False) # it is aslo not null
    extimated_hours = db.Column(db.String(30), nullable=False) # not null
    acctual_hour_spend = db.Column(db.String(30), nullable=False) # not null
    comments = db.Column(db.String(120))  
    status = db.Column(db.String(30), nullable=False) # not null
    assign_to = db.Column(db.String(30), nullable=False) # not null
    project1 = db.Column(db.String(30), nullable=False) # not null
    compby_date = db.Column(db.String(30))
    proj_id = db.Column(db.Integer, db.ForeignKey('projects.id'), nullable=False) # relation of foreignkey
    
    def __repr__(self):
        return f"Tasks('{self.id}','{self.task_title}', '{self.task_description}','{self.extimated_hours}','{self.acctual_hour_spend}','{self.comments}','{self.status}','{self.assign_to}','{self.project1}','{self.compby_date}')"


