from asyncio import tasks
from flask import render_template ,url_for, flash, redirect, request # this all are importing from flask
from flaskblog import app ,db  # app and db importing from pakage flaskblog
from flaskblog.forms import ProjectForm,TaskForm # importing from froms under the package flaskblog
from flaskblog.model import Projects, Tasks # importing from flaskblog
 
# this is showing all projects and tasks in like post
@app.route("/saprojects")
def saprojects():
    tasks = Tasks.query.all()
    return render_template('home.html', title = 'Home',tasks=tasks)
# this is home route
@app.route("/")
def home():
    tasks = Tasks.query.all()
    heading = ("slno.","Title" ,"Description","Estimated hours" ,"Actual Hours spent", "Comments", "Status" ,"Assigned to Project ","Project Id","Complete by date","Edit")
    return render_template('all_tasks.html', title = 'home',tasks = tasks,heading = heading)

    
# this is about route
@app.route("/about")
def about():
    return render_template('about.html', title='About')

# this route for creating project
@app.route("/project", methods=['GET', 'POST'])
def project():
    form = ProjectForm()
    if form.validate_on_submit():
        proj = Projects(proj_name=form.proj_name.data, description =form.description.data, start_date = form.start_date.data , end_date = form.end_date.data, status = form.status.data )
        db.session.add(proj)
        db.session.commit()
        flash(f'{form.proj_name.data}! Project created sucessfully ', 'success')
        return redirect(url_for('all_projects'))
    return render_template('project.html', title='New Project', form=form,legend = 'New - Project')

# this route for creating tasks
@app.route("/tasks", methods=['GET', 'POST'])
def tasks():
    form =TaskForm()
    if form.validate_on_submit():
        task = Tasks(task_title=form.task_title.data, task_description =form.task_description.data, extimated_hours = form.estimate_hr.data , acctual_hour_spend = form.acctual_hr.data, comments = form.comments.data, status = form.status.data ,assign_to = form.assign_to.data, project1 = form.project.data, compby_date = form.completed_date.data, proj_id = form.project.data)
        db.session.add(task)
        db.session.commit()
        flash(f'{form.task_title.data}! Task is created sucessfully ', 'success')
        return redirect(url_for('all_tasks'))
    return render_template('tasks.html', title='New Tasks', form = form, legend = 'New - Tasks')

# this route for showing single project and there is option for update
@app.route("/project/<int:projec_id>")
def project1(projec_id):
    project = Projects.query.get_or_404(projec_id)
    return render_template('project1.html',project=project)

# this route for project update 
@app.route("/project/<int:projec_id>/project_update", methods=['GET', 'POST'])
def project_update(projec_id):
    post = Projects.query.get_or_404(projec_id)
    form = ProjectForm()
    if form.validate_on_submit():
       post.proj_name = form.proj_name.data
       post.description = form.description.data
       post.start_date = form.start_date.data
       post.end_date = form.end_date.data
       post.status = form.status.data
       db.session.commit()
       flash('Your Project has been updated','success')
       return redirect(url_for('all_projects',projec_id = projec_id))
    elif request.method == 'GET':
        form.proj_name.data = post.proj_name
        form.description.data = post.description
        form.start_date.data = post.start_date
        form.end_date.data = post.end_date
        form.status.data= post.status
    return render_template('project.html', title='Update Project', form = form, legend = 'Update - Praject')


# this route for task update 
@app.route("/task/<int:taskss_id>")
def task1(taskss_id):
    post1 = Tasks.query.get_or_404(taskss_id)
    return render_template('task1.html',post=post1) 

# this route for showing single task and there is a button for update task
@app.route("/task/<int:taskss_id>/update_task" , methods=['GET', 'POST'])
def task_update(taskss_id):
    post = Tasks.query.get_or_404(taskss_id)
    form = TaskForm()
    if form.validate_on_submit():
       post.task_title = form.task_title.data
       post.task_description = form.task_description.data
       post.extimated_hours = form.estimate_hr.data
       post.acctual_hour_spend = form.acctual_hr.data
       post.comments = form.comments.data
       post.status = form.status.data
       post.assign_to = form.assign_to.data
       post.project1 = form.project.data
       post.compby_date = form.completed_date.data
       db.session.commit()
       flash('Your Task has been updated','success')
       return redirect(url_for('all_tasks',taskss_id = post.id))
    elif request.method == 'GET':
       form.task_title.data = post.task_title
       form.task_description.data = post.task_description
       form.estimate_hr.data = post.extimated_hours
       form.acctual_hr.data = post.acctual_hour_spend
       form.comments.data = post.comments
       form.status.data = post.status
       form.assign_to.data = post.assign_to
       form.project.data = post.project1
       form.completed_date.data = post.compby_date
    return render_template('tasks.html', title='Update Task', form = form, legend = 'Update - Task')

# this route for showing all projects
@app.route("/all_projects")
def all_projects():
    projects = Projects.query.all()
    heading = ("slno","Name","Description","Start-Date","End-Date","Status","Edit")
    return render_template('all_projects.html', title = 'all_projects',projects = projects,heading = heading)

# this routes for showing all tasks
@app.route("/all_tasks")
def all_tasks():
    tasks = Tasks.query.all()
    heading = ("slno.","Title" ,"Description","Estimated hours" ,"Actual Hours spent", "Comments", "Status" ,"Assigned to Project ","Project Id","Complete by date","Edit")
    return render_template('all_tasks.html', title = 'all_tasks',tasks = tasks,heading = heading)

