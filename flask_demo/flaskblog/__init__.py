from flask import Flask   #importing Flask in flask
from flask_sqlalchemy import SQLAlchemy  # importing SQLAlchemy 
from flask_bcrypt import Bcrypt
# creating the instance of our database
app = Flask(__name__)
app.config['SECRET_KEY'] = '5791628bb0b13ce0c676dfde280ba245'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///site.db'
db = SQLAlchemy(app) 
bcrypt = Bcrypt(app)


from flaskblog import routes
 
